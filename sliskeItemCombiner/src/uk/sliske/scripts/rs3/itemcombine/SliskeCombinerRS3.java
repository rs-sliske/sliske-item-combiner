package uk.sliske.scripts.rs3.itemcombine;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import org.powerbot.script.Condition;
import org.powerbot.script.Script;
import org.powerbot.script.rt6.Widget;

import uk.sliske.scripts.rs3.Debug;
import uk.sliske.scripts.rs3.SliskeScript;
import uk.sliske.scripts.rs3.util.ItemGroup;
import uk.sliske.scripts.rs3.wrappers.ClientContext;


@Script.Manifest(
		name = "Sliskes Item Combiner", 
		description = "Combines items while you fap.", 
		properties="topic=1235276;client=6")
public class SliskeCombinerRS3 extends SliskeScript<ClientContext> {

	private ArrayList<ItemGroup>	items;

	private int						amountOfItemsInInvent	= 0;
	private final Widget			craftingIntertface;

	private enum STATE {
		BANKING, USEING, WAITING, IDLE, ERROR;
	}

	public SliskeCombinerRS3() {
		super(true);
		craftingIntertface = ctx.widgets.widget(1370);
	}

	@Override
	public void start() {
		Debug.println("starting script");
		items = ctx.invent.inventoryList();
		if (items.size() < 2) {
			loadGUI();
		}
		new Thread() {
			public void run() {
				for (ItemGroup i : items) {
					if (ctx.cacheLoadedSuccessfully()) {
						Debug.println(ctx.getItemInfo(i.getID()).name + " : " + i.getAmount());
					}
					amountOfItemsInInvent += i.getAmount();
				}
			}
		}.start();

		running = true;
	}
	
	

	@Override
	public void poll() {
		STATE state = state();
		switch (state) {
			case BANKING:
				if (ctx.bank.opened()) {
					int fails = 0;
					if (!ctx.invent.isEmpty()) {
						while (!ctx.bank.depositInventory()) {
							fails++;
							if (fails > 5) {
								return;
							}
						}
					}
					Condition.wait(new Callable<Boolean>() {
						@Override
						public Boolean call() throws Exception {
							return ctx.invent.isEmpty();
						}

					});
					for (final ItemGroup i : items) {
						int c = ctx.bank.countItem(i.getID());
						if (ctx.cacheLoadedSuccessfully()) {
							Debug.println("bank contains " + c + " " + ctx.getItemInfo(i.getID()).name);
						}
						if (ctx.bank.countItem(i.getID()) > i.getAmount()) {
							ctx.bank.withdraw(i.getID(), i.getAmount());
							Condition.wait(new Callable<Boolean>() {
								@Override
								public Boolean call() throws Exception {
									return ctx.invent.count(i.getID()) == i.getAmount();
								}

							});
						} else {
							running = false;
							error("bank does not contain required items");
						}
					}
					ctx.bank.close();
				} else {
					ctx.bank.open();
				}
			case USEING:
				ctx.invent.select().id(items.get(0).getID()).poll().click("Use");
				Condition.wait(new Callable<Boolean>() {
					@Override
					public Boolean call() throws Exception {
						return ctx.invent.itemSelected();
					}
				});
				ctx.invent.select().id(items.get(1).getID()).poll().click("Use");
				Condition.wait(new Callable<Boolean>() {
					@Override
					public Boolean call() throws Exception {
						return craftingIntertface.component(38).visible();
					}
				});
				ctx.input.send(" ");
				Condition.wait(new Callable<Boolean>() {
					@Override
					public Boolean call() throws Exception {
						int i = 0;
						for (ItemGroup ig : items) {
							i += ctx.invent.count(ig.getID());
						}
						return i < amountOfItemsInInvent;
					}
				});
			default:
				Condition.sleep(genRandWait(100, 5));
		}

	}

	private STATE state() {
		if (!running)
			return STATE.IDLE;
		if (!ctx.players.local().idle()) {
			return STATE.WAITING;
		}
		if (ctx.invent.count(items.get(1).getID()) == items.get(1).getAmount()) {
			return STATE.USEING;
		}
		if (ctx.bank.inViewport() && ctx.invent.count(items.get(1).getID()) == 0) {
			return STATE.BANKING;
		}
		return STATE.ERROR;
	}

	
	

}
